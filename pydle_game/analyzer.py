#!/usr/bin/env python3
# -*- coding=utf-8 -*-
"""
Module to inspect possible words and find useful information
"""

import json
from collections import Counter
from pathlib import Path
from typing import Dict, List, Optional

FilePath = Optional[Path | str]


def load_words(word_path: FilePath = None) -> List[str]:
    # _default_word_file = Path(__file__).parent.absolute() / "data" / "words.json"
    _default_word_path = Path(__file__).parent.absolute() / "data" / "words.json"
    word_file = Path(word_path) if word_path else _default_word_path
    with word_file.open("rt") as word_file:
        words = json.load(word_file)
    return words


def _sort_dict(counter: Counter) -> Counter:
    return Counter(
        {
            k: v
            for k, v in sorted(counter.items(), key=lambda item: item[1], reverse=True)
        }
    )


def get_all_letter_counts(words: List[str]) -> Counter:
    letter_counts = Counter()
    for word in words:
        letter_counts += Counter(word)
    return _sort_dict(letter_counts)


def get_unique_letter_counts(words: List[str]) -> Counter:
    letter_counts = Counter()
    for word in words:
        word_letters = Counter(set(word))
        letter_counts += word_letters
    return _sort_dict(letter_counts)


def get_most_common_letters(words: List[str], num_letters: int = 10) -> List[str]:
    letter_counts = get_unique_letter_counts(words)
    most_common = set([letter[0] for letter in letter_counts.most_common(num_letters)])
    best_words = []
    for word in words:
        found = most_common.difference(set(word))
        if len(found) <= 5:
            best_words.append(word)
    return best_words


def max_vowels(words: List[str]):
    vowels = set("aeiouy")
    best = get_most_common_letters(words)
    # diff = len(vowels)
    answer = []
    for word in best:
        # if (num := len(vowels.difference(word))) < diff:
        #     answer = word
        #     diff = num
        diff = set(word).difference(vowels)
        if len(diff) < 3:
            print(f"{word}: {diff}")
            answer.append(word)

    # print(diff)
    # print(answer)


def find_min_difference(words: List[str]):
    pass


if __name__ == "__main__":
    words = load_words()
    # print(f"WORD COUNT {len(words)}")
    # all_letters = get_all_letter_counts(words)
    # print("============== ALL LETTERS ==============")
    # # print(dict(all_letters))
    # # print(json.dumps(sorted_dict, indent=2))
    # unique_letters = get_unique_letter_counts(words)
    # print("============== UNIQUE LETTERS ==============")
    # print(json.dumps(unique_letters, indent=2))
    # print(dict(unique_letters))
    print(max_vowels(words))
