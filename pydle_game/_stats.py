#!/usr/bin/env python3
# -*- coding=utf-8 -*-
"""
Pydle game player statistics
"""

import json
import logging
from collections import Counter
from pathlib import Path
from typing import List, Optional

from . import app_paths

logging.basicConfig(filename=app_paths.log_file_path, level=logging.INFO)
_logger = logging.getLogger(__name__)

StatData = List[dict]


class Stats:
    def __init__(self):
        self._datastore: Path = Path(app_paths.app_data_path) / "player_stats.jsonl"
        self._stats: Optional[StatData] = None

    def update(self, game_stats: dict) -> None:
        """Update player stats with new game"""
        with self._datastore.open("a+") as fileobj:
            print(json.dumps(game_stats), file=fileobj)

    def get_stats(self) -> StatData:
        """Read player stats file"""
        if not self._stats:
            _stats = []
            if not self._datastore.exists():
                _logger.error("Stats file %s does not exist", self._datastore)
                raise FileNotFoundError(f"{self._datastore} does not exist")
            with self._datastore.open("r") as fileobj:
                for line in fileobj.readlines():
                    _stats.append(json.loads(line))
            self._stats = _stats
        return self._stats

    def parse_stats(self, data: Optional[StatData] = None) -> dict:
        """Parse stats file for total info"""
        data = data or self.get_stats()
        try:
            stats_dict = {
                "games": len(data),
                "wins": sum(1 for row in data if row.get("found", False) == True),
                "attempts": Counter([row.get("attempts") for row in data if row.get("found")]),
            }
        except KeyError as kerr:
            _logger.error("Unable to parse stats. Invalid format", exc_info=kerr)
            raise kerr
        except Exception as err:
            _logger.error("Unable to parse status. Unknown err", exc_info=err)
            raise err
        return stats_dict


if __name__ == "__main__":
    print("This is not the py you seek")
