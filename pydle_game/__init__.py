from appdata import AppDataPaths

app_paths = AppDataPaths("pydle")
app_paths.setup()

from . import analyzer
from .cli import run
from .game import Pydle
