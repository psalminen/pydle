# Pydle Game
## Wordle in the terminal!

Terminal based wordle game written in python

## How to Install
Pydle can be installed from pypi using pip:
```bash
    pip install pydle_game
```

## How to run
During installation, a script will be added to your path. To run, just run `pydle`
Pydle can also be run without installing as a package by cloning [this repository](https://gitlab.com/psalminen/pydle)
```bash
    git clone https://gitlab.com/psalminen/pydle
    cd pydle
    python -m pydle
```

## Requirements
+ appdata
+ rich
+ termplotlib (only needed to show attempts bar graph at end)


