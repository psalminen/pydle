#!/usr/bin/env python
"""
The only reason for the existence of this setup.py shim is to support editable
installs while developing and testing this package.
"""
import setuptools

if __name__ == "__main__":
    setuptools.setup()
